<?php

namespace AppBundle\Services;


class WorkingDaysService
{

    public function getWorkingDays($first, $second, array $DaysOff){

        $count = 0;
        $numbersDaysOff = array(6, 7);
        $current = strtotime($first);
        $second = strtotime($second);

        while ($current < $second)
        {

            // Condition for days off
            if(!(in_array(date('N', $current), $numbersDaysOff))){
                $count++;
            }

            //Condition for other holidays
            if(in_array($current, $DaysOff)){
                $count--;
            }

            $current = strtotime('+1 day', $current);
        }

        return $count;
    }

}