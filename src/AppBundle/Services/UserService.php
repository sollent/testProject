<?php

namespace AppBundle\Services;


class UserService extends MainService
{

    private $uniqueValues = array(
        array(
            'id' => 1,
            'name' => 'some',
            'value' => 'Hello man!'
        ),
        array(
            'id' => 2,
            'name' => 'come',
            'value' => 'How is live?'
        ),
        array(
            'id' => 3,
            'name' => 'hello',
            'value' => 'Next344'
        ),
        array(
            'id' => 4,
            'name' => 'pasha',
            'value' => 'How it is live?!'
        ),
        array(
            'id' => 5,
            'name' => 'mister',
            'value' => 'lorem and i can do it'
        ),
        array(
            'id' => 6,
            'name' => 'some info',
            'value' => 'This is a temple for some info'
        ),
        array(
            'id' => 7,
            'name' => 'Hai',
            'value' => 'Mister Hai wait you actions'
        ),
        array(
            'id' => 8,
            'name' => 'Go to',
            'value' => 'Go to the cinema today))'
        ),
        array(
            'id' => 9,
            'name' => 'Click me',
            'value' => 'What are yu doing man?'
        ),
         array(
            'id' => 10,
            'name' => 'Into',
            'value' => 'Hello man!Hello man!Hello man!'
        ),
        array(
            'id' => 11,
            'name' => 'Page',
            'value' => 'How is live?'
        )
    );


    /**
     * @return array
     */
    public function findAll()
    {

//        $strings = array();
//
//        for ($i = 0; $i < count($this->uniqueValues); $i++)
//        {
//            array_push($strings, $this->uniqueValues[$i]['value']);
//        }

        return $this->uniqueValues;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findOneById($id)
    {

        for ($i = 0; $i < count($this->uniqueValues); $i++)
        {
            if($this->uniqueValues[$i]["id"] == $id)
            {
                return $this->uniqueValues[$i];
            }


        }

        return null;

    }

    public function findOneBySlug($slug)
    {

        for ($i = 0; $i < count($this->uniqueValues); $i++)
        {
            if($slug == $this->uniqueValues[$i]['value'])
            {
                return $this->uniqueValues[$i]['value'];
            }

        }

        return null;

    }

}
