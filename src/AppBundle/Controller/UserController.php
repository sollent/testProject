<?php

namespace AppBundle\Controller;

use AppBundle\Services\UserService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends MainController
{

    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    // Route "/admin/{page}/{pageId} has a common structure with route "/admin/new" of the editAction
    // and I made the pageId parameter required so that the routes don't intersect"
    /**
     * @Route("/admin/{page}/{pageId}", methods={"GET"},
     *  name="index-action", requirements={"page"="[a-zA-Z]+$", "pageId"="\d+"})
     * @param Request $request
     * @param $page
     * @param int $pageId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $page = null, $pageId)
    {

        $response = $this->userService->findAll();
//        echo "<pre>";
//        print_r($response);
//        exit();
        return $this->render('default/user/index.html.twig', [
            'array' => $response
        ]);

    }


    /**
     * @Route("/admin/{id}", methods={"GET"}, name="show-action",
     * requirements={"id"="\d+"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, $id)
    {

        $response = $this->userService->findOneById($id);

        if($response == null){
            return new Response("This page not found!", 404);
        }

        return $this->render('default/user/show.html.twig', [
            'string' => $response
        ]);

    }

    /**
     * @Route("/admin/new", methods={"GET", "POST"}, name="edit-action-first")
     * @Route("/admin/{id}/edit", methods={"GET", "POST"}, name="edit-action-second",
     * requirements={"id"="\d+"})
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id = 0)
    {
        if($id > 0)
        {
            $response = $this->userService->findOneById($id);

            if($response == null){
                return new Response("This page not found!", 404);
            }

            return $this->render('default/user/edit.html.twig', [
                'string' => $response
            ]);
        } else {
            return $this->render('default/user/edit.html.twig', [
                'string' => 'New'
            ]);
        }

    }

    /**
     * @Route("/admin/{id}/delete", methods={"DELETE"}, name="delete-action",
     * requirements={"id"="\d+"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, $id)
    {

        $response = $this->userService->findAll();

        if($response == null){
            return new Response("This page not found!", 404);
        }

        $message = "Message with id = " . $id . " has been successful deleted";

        return $this->render('default/user/index.html.twig', [
           'message' =>  $message,
            'array' => $response
        ]);

    }
}