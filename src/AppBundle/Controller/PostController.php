<?php

namespace AppBundle\Controller;


use AppBundle\Services\PostService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends MainController
{

    private $postService;

    /**
     * PostController constructor.
     * @param PostService $postService
     */
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    // Route "/admin/{page}/{pageId} has a common structure with route "/admin/new" of the editAction
    // and I made the pageId parameter required so that the routes don't intersect"
    /**
     * @Route("/admin/user/{page}/{pageId}", methods={"GET"}, name="index-action-post",
     * requirements={"page"="[a-zA-Z]+$", "pageId"="\d+"})
     * @param Request $request
     * @param null $page
     * @param int $pageId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $page = null, $pageId)
    {
        $response = $this->postService->findAll();

        return $this->render('default/post/index.html.twig', [
            'array' => $response
        ]);

    }

    /**
     * @Route("/admin/user/{id}", methods={"GET"}, name="show-action-post",
     * requirements={"id"="\d+"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, $id)
    {

        $response = $this->postService->findOneById($id);

        if($response == null){
            return new Response("This page not found!", 404);
        }

        return $this->render('default/post/show.html.twig', [
            'string' => $response
        ]);

    }

    /**
     * @Route("/admin/user/new", methods={"GET", "POST"}, name="edit-action-post-first")
     * @Route("/admin/user/{id}/edit", methods={"GET", "POST"}, name="edit-action-post-second",
     * requirements={"id"="\d+"})
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id = 0)
    {

        if($id > 0)
        {
            $response = $this->postService->findOneById($id);

            if($response == null){
                return new Response("This page not found!", 404);
            }

            return $this->render('default/post/edit.html.twig', [
                'string' => $response
            ]);
        } else {
            return $this->render('default/post/edit.html.twig', [
                'string' => 'New'
            ]);
        }

    }

    /**
     * @Route("/admin/user/{id}/delete", methods={"DELETE"}, name="delete-action-post",
     * requirements={"id"="\d+"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, $id)
    {

        $response = $this->postService->findAll();

        if($response == null){
            return new Response("This page not found!", 404);
        }

        $message = "Message with id = " . $id . " has been successful deleted";

        return $this->render('default/post/index.html.twig', [
            'message' => $message,
            'array' => $response
        ]);

    }

    /**
     * @Route("/post/{pageId}", methods={"GET"}, name="list-action",
     * requirements={"pageId"="\d+"})
     * @param Request $request
     * @param $pageId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request, $pageId = 1)
    {

        $response = $this->postService->findAll();

        return $this->render('default/post/list.html.twig', [
            'array' => $response
        ]);

    }

    /**
     * @Route("/post/{slug}", methods={"GET"}, name="view-action",
     * requirements={"slug"="[a-zA-Z]+$"})
     * @param Request $request
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Request $request, $slug)
    {

        // Parameter $slug is the 'title' in the request url

        $response = $this->postService->findOneBySlug($slug);

        if($response == null){
            return new Response("This page not found!", 404);
        }

        return $this->render('default/post/view.html.twig', [
            'string' => $response
        ]);

    }

}